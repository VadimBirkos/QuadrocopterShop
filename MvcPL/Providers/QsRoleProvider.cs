﻿using System.Web.Security;
using BLL.Interface.Services;

namespace MvcPL.Providers
{
    public class QsRoleProvider:RoleProvider
    {
        private readonly IUserService _userService;

        public QsRoleProvider()
        {
            //_userService = System.Web.Mvc.DependencyResolver.Current.GetService<IUserService>();
              _userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
        }

        public QsRoleProvider(IUserService userService)
        {
            _userService = userService;
        }

        public override bool IsUserInRole(string userName, string roleName)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(userName);
            if (user == null) return false;
            return user.Role.Title == roleName;
        }

        public override string[] GetRolesForUser(string userName)
        {
            var roles = new string[] { };
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(userName);
            if (user == null) return roles;
            if (user.Role != null) roles = new[] { user.Role.Title };
            return roles;
        }
        #region Stubs

        public override void CreateRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new System.NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new System.NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new System.NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new System.NotImplementedException();
        }

        public override string ApplicationName { get; set; }

#endregion
    }
}