﻿using System;
using System.Web.Helpers;
using System.Web.Security;
using BLL.Interface.Entities;
using BLL.Interface.Services;

namespace MvcPL.Providers
{
    public class QsMembershipProvider:MembershipProvider
    {
        private readonly IUserService _userService;

        public QsMembershipProvider()
        {
            _userService = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
        }

        public QsMembershipProvider(IUserService userService)
        {
            _userService = userService;
        }

        public MembershipUser CreateUser(string username, string password)
        {
            var user = new User()
            {
                Login = username,
                Password = Crypto.HashPassword(password),
                Role = new Role() { Id = 2 },
                Basket = new Basket(),
                Created = DateTime.Now
            };
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            try { svc.Create(user); }
            catch (InvalidOperationException) { return null; }
            var membershipUser = new MembershipUser("QsMembershipProvider",
                user.Login, null, null, null, null, false, false,
                user.Created, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue);
            return membershipUser;
        }

        public override bool ValidateUser(string login, string password)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(login);
            return user != null && Crypto.VerifyHashedPassword(user.Password, password);
        }

        public override MembershipUser GetUser(string login, bool userIsOnline)
        {
            var svc = (IUserService)System.Web.Mvc.DependencyResolver.Current.GetService(typeof(IUserService));
            var user = svc.GetByLogin(login);
            if (user == null) return null;
            var memberUser = new MembershipUser("QsMembershipProvider",
                user.Login, null, null, null, null, false, false,
                user.Created, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue);
            return memberUser;
        }

        #region Stubs
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer,
            bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new System.NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion,
            string newPasswordAnswer)
        {
            throw new System.NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new System.NotImplementedException();
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new System.NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new System.NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new System.NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new System.NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new System.NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new System.NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new System.NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new System.NotImplementedException();
        }

        public override bool EnablePasswordRetrieval { get; }
        public override bool EnablePasswordReset { get; }
        public override bool RequiresQuestionAndAnswer { get; }
        public override string ApplicationName { get; set; }
        public override int MaxInvalidPasswordAttempts { get; }
        public override int PasswordAttemptWindow { get; }
        public override bool RequiresUniqueEmail { get; }
        public override MembershipPasswordFormat PasswordFormat { get; }
        public override int MinRequiredPasswordLength { get; }
        public override int MinRequiredNonAlphanumericCharacters { get; }
        public override string PasswordStrengthRegularExpression { get; }

#endregion
    }
}