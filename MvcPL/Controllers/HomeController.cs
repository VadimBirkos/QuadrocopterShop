﻿using System.Linq;
using System.Web.Mvc;
using BLL.Interface.Services;
using MvcPL.ViewModels;

namespace MvcPL.Controllers
{
    public class HomeController : Controller
    {

        private readonly IBasketService _basketService;
        private readonly IBrandService _brandService;
        private readonly ICountryService _countryService;
        private readonly ICameraPresenceService _cameraPresenceService;
        private readonly IQuadrocopterService _quadrocopterService;
        private readonly IUserService _userService;

        public HomeController(IBasketService basketService, ICameraPresenceService cameraPresenceService,
            ICountryService countryService, IBrandService brandService, IQuadrocopterService quadrocopterService,
            IUserService userService)
        {
            _basketService = basketService;
            _cameraPresenceService = cameraPresenceService;
            _countryService = countryService;
            _brandService = brandService;
            _quadrocopterService = quadrocopterService;
            _userService = userService;
        }

        public ActionResult Index(int page = 1, int pageSize = 6)
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = _userService.GetByLogin(User.Identity.Name);
                ViewBag.CountOfQuadrocopters = _basketService.GetBasketQuadrocopters(user.Basket.Id).Count();
            }
            var filterViewModel = InitializeFields();
            var quadrocopters = _quadrocopterService.GetAll().ToList();
            var pageInfo = new PageInfo {PageNumber = page, PageSize = pageSize, TotalItems = quadrocopters.Count};
            quadrocopters = quadrocopters.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            var model = new ShopMainViewModel
            {
                FilterPanelViewModel = filterViewModel,
                QuadrocoptersPagination = new QuadrocopterPaginationModel
                {
                    Quadrocopters = quadrocopters,
                    PageInfo = pageInfo
                }
            };
            return View(model);
        }

        public FilterPanelViewModel InitializeFields()
        {
            var quadrocopters = _quadrocopterService.GetAll().ToList();
            var bllCountry = _countryService.GetAll().ToList();
            var countryModel =
              bllCountry.Select(
                  country =>
                      new CountryViewModel()
                      {
                          Name = country.Name,
                          Id = country.Id,
                          Count = quadrocopters.Count(item => item.MadeCountry.Id == country.Id)
                      }).ToList();

            var cameraPresenceBll = _cameraPresenceService.GetAll().ToList();
            var cameraPresenceModel =
               cameraPresenceBll.Select(
                   camera =>
                       new CameraPresenseViewModel()
                       {
                           Type = camera.Type,
                           Id = camera.Id,
                           Count = quadrocopters.Count(item => item.CameraPresense.Id == camera.Id)
                       }).ToList();

            var brandBll = _brandService.GetAll().ToList();
            var brandModel =
             brandBll.Select(
                 brand =>
                     new BrandsViewModel()
                     {
                         Name = brand.Name,
                         Id = brand.Id,
                         Count = quadrocopters.Count(item => item.Brand.Id == brand.Id)
                     }).ToList();
            return new FilterPanelViewModel
            {
                Brands = brandModel,
                Countries = countryModel,
                CameraPresenses = cameraPresenceModel

            };
        }
    }
}