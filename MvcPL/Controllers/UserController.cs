﻿using System.IO;
using System.Web;
using System.Web.Mvc;
using BLL.Interface.Services;
using MvcPL.Infrastructure;

namespace MvcPL.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        private readonly IUserService _userService;
        private readonly IBasketService _basketService;
        public UserController(IUserService userService, IBasketService basketService)
        {
            _userService = userService;
            _basketService = basketService;
        }

        public ActionResult ViewProfile(string login)
        {
            var user = _userService.GetByLogin(login);
            if (user != null)
            {
                return View(user);
            }
            return RedirectToAction("NotFound", "Error");
        }

        [HttpGet]
        [QsAuthorize]
        public ActionResult UpdateImage(int id)
        {
                var user = _userService.GetById(id);
            if (user == null)
            {
                RedirectToAction("NotFound", "Error");
            }
            return View();
        }

        [HttpPost]
        [QsAuthorize]
        public ActionResult UpdateImage(HttpPostedFileBase  fileUpload)
        {
            var user = _userService.GetByLogin(HttpContext.User.Identity.Name);
            if (user == null) RedirectToAction("NotFound", "Error");
            if (fileUpload == null || fileUpload.ContentLength <= 0) return RedirectToAction("NotFound", "Error");

            var path = Path.Combine(Server.MapPath("~/Uploads/Accounts/"),
                  Path.GetFileName(fileUpload.FileName));
            fileUpload.SaveAs(path);
            user.AvatarPath = "/Uploads/Accounts/" + Path.GetFileName(fileUpload.FileName);
            _userService.Update(user);
            return RedirectToAction("ViewProfile", "User", new {login = user.Login});
        }

        [QsAuthorize]
        public ActionResult DeleteUser(int id)
        {
            var user = _userService.GetById(id);
            if (user == null) return RedirectToAction("NotFound", "Error");
            _basketService.Delete(_basketService.GetById(user.Basket.Id));
            _userService.Delete(user);
            return RedirectToAction("Index", "Home");
        }
    }
}
