﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using MvcPL.Infrastructure;
using MvcPL.ViewModels;

namespace MvcPL.Controllers
{
   
    public class QuadrocopterController : Controller
    {
        private readonly IBrandService _brandService;
        private readonly ICameraPresenceService _cameraPresenceService;
        private readonly ICountryService _countryService;
        private readonly IQuadrocopterService _quadrocopterService;

        public QuadrocopterController(IBrandService brandService, ICameraPresenceService cameraPresenceService,
            ICountryService countryService, IQuadrocopterService quadrocopterService)
        {
            _brandService = brandService;
            _cameraPresenceService = cameraPresenceService;
            _countryService = countryService;
            _quadrocopterService = quadrocopterService;
        }

        [QsAuthorize]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var countries = _countryService.GetAll();
            ViewBag.Countries = countries;

            var brands = _brandService.GetAll();
            ViewBag.Brands = brands;

            var camera = _cameraPresenceService.GetAll();
            ViewBag.CameraPres = camera;

            var quad = _quadrocopterService.GetById(id);
            ViewBag.Image = quad.ImagePath;
            var quadroModel = new CreateQuadrocopterViewModel()
            {
                Id = quad.Id,
                Article = quad.Article,
                BrandId = quad.Brand.Id,
                CameraPresenseId = quad.CameraPresense.Id,
                ControlDistance = quad.ControlDistance,
                Cost = quad.Cost,
                Description = quad.Description,
                MadeCountryId = quad.MadeCountry.Id,
                Name = quad.Name,
            };
            return View(quadroModel);
        }

        public ActionResult Edit(CreateQuadrocopterViewModel model)
        {
            return RedirectToRoute("/Home/Index");
        }

        public ActionResult ShowQuadrocopter(int quadroId)
        {
            var quadrocopter = _quadrocopterService.GetById(quadroId);
            
            return View(new QuadrocopterViewModel()
            {
                Article = quadrocopter.Article,
                Brand = quadrocopter.Brand.Name,
                CameraPresense = quadrocopter.CameraPresense.Type,
                ControlDistance = quadrocopter.ControlDistance,
                Cost = quadrocopter.Cost,
                Description = quadrocopter.Description,
                ImagePath = quadrocopter.ImagePath,
                MadeCountry = quadrocopter.MadeCountry.Name,
                Name = quadrocopter.Name
            });
        }

        [HttpPost]
        public ActionResult FilterQuadrocopers(FilterParametr[] Filter , int page = 1, int pageSize = 6)
        {
            var qadrocopters = this.Filter(Filter).ToList();
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = qadrocopters.Count };
            qadrocopters = qadrocopters.Skip((page - 1) * pageSize).Take(pageSize).ToList();
           
            var model = new QuadrocopterPaginationModel
            {
                Quadrocopters = qadrocopters,
                PageInfo =pageInfo
            };
            return PartialView("QuadrocoptersView", model);
        }


        public ActionResult Index(int page = 1, int pageSize = 6)
        {
            var quadrocopters = _quadrocopterService.GetAll().ToList();
            var pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = quadrocopters.Count };
            quadrocopters = quadrocopters.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            var model = new QuadrocopterPaginationModel
            {
                Quadrocopters = quadrocopters,
                PageInfo = pageInfo
            };
            return PartialView("QuadrocoptersView", model);
        }
        
        [HttpGet]
        [QsAuthorize]
        public ActionResult AddQuadrocopter()
        {
            var countries = new SelectList(_countryService.GetAll(), "Id", "Name");
            ViewBag.Countries = countries;

            var brands = new SelectList(_brandService.GetAll(), "Id", "Name");
            ViewBag.Brands = brands;

            var camera = new SelectList(_cameraPresenceService.GetAll(), "Id", "Type");
            ViewBag.CameraPres = camera;
            return View(new CreateQuadrocopterViewModel());
        }

        [HttpPost]
        [QsAuthorize]
        public ActionResult AddQuadrocopter(CreateQuadrocopterViewModel model)
        {
            var quadrocopter = new Quadrocopter()
            {
                Article = model.Article,
                Brand = _brandService.GetById(model.BrandId),
                CameraPresense = _cameraPresenceService.GetById(model.CameraPresenseId),
                MadeCountry = _countryService.GetById(model.MadeCountryId),
                ControlDistance = model.ControlDistance,
                Cost = model.Cost,
                Description = model.Description,
                Name = model.Name
            };

            if (model.UploadedPreviewImage == null || model.UploadedPreviewImage.ContentLength <= 0)
                return RedirectToAction("NotFound", "Error");

            var path = Path.Combine(Server.MapPath("~/Uploads/Quadrocopters/"),
                Path.GetFileName(model.UploadedPreviewImage.FileName));
            model.UploadedPreviewImage.SaveAs(path);
            quadrocopter.ImagePath = "/Uploads/Quadrocopters/" + Path.GetFileName(model.UploadedPreviewImage.FileName);
            _quadrocopterService.Create(quadrocopter);

            return RedirectToAction("Index", "Home");
        }

        public IEnumerable<Quadrocopter> Filter(FilterParametr[] parametrs)
        {
            var allQuadrocopters = _quadrocopterService.GetAll().ToList();
            var quadrocoptersResult = new  List<Quadrocopter>();
            foreach (var parametr in parametrs)
            {
                switch (parametr.Critery)
                {
                     case "cameraPres":
                    {
                        quadrocoptersResult.AddRange(allQuadrocopters.Where(item=>item.CameraPresense.Type == parametr.Value));
                        break;
                    }
                    case "country":
                        {
                            quadrocoptersResult.AddRange(allQuadrocopters.Where(item => item.MadeCountry.Name == parametr.Value));
                            break;
                        }
                    case "brand":
                    {
                            quadrocoptersResult.AddRange(allQuadrocopters.Where(item => item.Brand.Name == parametr.Value));
                            break;
                    }

                }
            }

            return quadrocoptersResult.Distinct();
        }
    }
}