﻿using System.Web.Mvc;
using System.Web.Security;
using BLL.Interface.Services;
using MvcPL.Infrastructure;
using MvcPL.Providers;
using MvcPL.ViewModels;

namespace MvcPL.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View(new RegisterViewModel());
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Ошибка регистрации");
                return View();
            }
            var membershipUser =
                ((QsMembershipProvider)Membership.Provider)
                    .CreateUser(viewModel.Login, viewModel.Password);
            if (membershipUser == null)
            {
                ModelState.AddModelError("Login", "Данный логин недоступен");
                return View(viewModel);
            }
            FormsAuthentication.SetAuthCookie(viewModel.Login, false);
            ViewBag.Login = HttpContext.User.Identity.Name;
            ViewBag.Role = HttpContext.User.IsInRole("User");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult SignIn()
        {
            if (User.Identity.Name != null)
            {
                
            }

            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInViewModel viewModel, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(viewModel.Login, viewModel.Password))
                {
                    FormsAuthentication.SetAuthCookie(viewModel.Login, viewModel.RememberMe);
                    if (Url.IsLocalUrl(returnUrl)) return Redirect(returnUrl);
                    ViewBag.Login = HttpContext.User.Identity.Name;
                    ViewBag.Role = HttpContext.User.IsInRole("User");
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Неверный логин или пароль");
            }
            
            return View(viewModel);
        }

        [QsAuthorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("SignIn");
        }
    }
}