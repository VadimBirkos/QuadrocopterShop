﻿using System.Linq;
using System.Web.Mvc;
using BLL.Interface.Services;
using MvcPL.Infrastructure;

namespace MvcPL.Controllers
{
    public class BasketController : Controller
    {
        private readonly IBasketService _basketService;
        private readonly IQuadrocopterService _quadrocopterService;
        private readonly IUserService _userService;


        public BasketController(IBasketService basketService, IQuadrocopterService quadrocopterService, IUserService userService)
        {
            _basketService = basketService;
            _quadrocopterService = quadrocopterService;
            _userService = userService;
        }

        [HttpGet]
        public int GetCountOfItems()
        {
            if (!User.Identity.IsAuthenticated) return 0;
            var user = _userService.GetByLogin(User.Identity.Name);
            return _basketService.GetBasketQuadrocopters(user.Basket.Id).Count();
        }

        [QsAuthorize]
        public ActionResult AddToBusket(string userName, int quadrocopterId)
        {
            var quadrocopter = _quadrocopterService.GetById(quadrocopterId);
            var user = _userService.GetByLogin(userName);
            ViewBag.CountOfQuadrocopters = _basketService.GetBasketQuadrocopters(user.Basket.Id).Count();
            _basketService.AddInBusket(user.Basket.Id,quadrocopter);
            return RedirectToAction("Index", new {basketId = user.Basket.Id});

        }

        // GET: Basket
        [QsAuthorize]
        public ActionResult Index()
        {
            var user = _userService.GetByLogin(User.Identity.Name);
            var basket = _basketService.GetById(user.Basket.Id);
            return View(basket);
        }

        [QsAuthorize]
        public ActionResult Delete(int quadroId, int basketId)
        {
            var basket = _basketService.GetById(basketId);
            _basketService.DeleteQuadrocopter(basketId, quadroId);
            return RedirectToAction("Index");
        }
    }
}