﻿using System.ComponentModel.DataAnnotations;
using BLL.Interface.Entities;

namespace MvcPL.ViewModels
{
    public class QuadrocopterViewModel
    {
        public int Id { get; set; }
        [Display( Name = "Название")]
        public string Name { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Display(Name = "Стоимость")]
        public double Cost { get; set; }

        public string ImagePath { get; set; }
        [Display(Name = "Артикль")]
        public int Article { get; set; }
        [Display(Name = "Дальность управления")]
        public int ControlDistance { get; set; }
        [Display(Name = "Наличие камеры")]
        public string CameraPresense { get; set; }
        [Display(Name = "Брэнд")]
        public string Brand { get; set; }
        [Display(Name = "Страна изготовитель")]
        public string MadeCountry { get; set; }
    }
}