﻿using System.Collections.Generic;
using BLL.Interface.Entities;

namespace MvcPL.ViewModels
{
    public class FilterPanelViewModel
    {
        public IEnumerable<BrandsViewModel> Brands { get; set; }
        public IEnumerable<CountryViewModel> Countries { get; set; }
        public IEnumerable<CameraPresenseViewModel> CameraPresenses { get; set; }
    }
}