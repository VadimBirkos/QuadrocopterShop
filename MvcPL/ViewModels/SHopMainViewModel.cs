﻿namespace MvcPL.ViewModels
{
    public class ShopMainViewModel
    {
        public FilterPanelViewModel FilterPanelViewModel { get; set; }
        public QuadrocopterPaginationModel QuadrocoptersPagination { get; set; }
    }
}