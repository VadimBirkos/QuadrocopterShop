﻿namespace MvcPL.ViewModels
{
    public class BrandsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }
    }
}