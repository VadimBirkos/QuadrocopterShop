﻿namespace MvcPL.ViewModels
{
    public class ProfileViewModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string AvatarPath { get; set; }
    }
}