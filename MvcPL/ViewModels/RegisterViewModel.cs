﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MvcPL.ViewModels
{
    public class RegisterViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }

        [Display(Name = "Логин:")]
        [Required(ErrorMessage = "Вы должны ввести логин!")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Вы должны ввести пароль!")]
        [StringLength(100, ErrorMessage = "Пароль должен содержать {2} и более символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Подтвердите Ваш пароль!")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтвердите пароль:")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают!")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.Date)]
        public DateTime AddedDate { get; set; }
    }
}