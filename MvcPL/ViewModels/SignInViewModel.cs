﻿using System.ComponentModel.DataAnnotations;

namespace MvcPL.ViewModels
{
    public class SignInViewModel
    {
        [Display(Name = "Логин:")]
        [Required(ErrorMessage = "Вы должны ввести логин!")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Вы должны ввести пароль!")]
        [StringLength(100, ErrorMessage = "Пароль должен содержать {2} or more symbols.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль:")]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }
    }
}