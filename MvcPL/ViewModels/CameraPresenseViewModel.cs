﻿namespace MvcPL.ViewModels
{
    public class CameraPresenseViewModel
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public int Count { get; set; }
    }
}