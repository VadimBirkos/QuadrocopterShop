﻿using System;
using System.Collections.Generic;
using BLL.Interface.Entities;

namespace MvcPL.ViewModels
{
    public class QuadrocopterPaginationModel
    {
        public PageInfo PageInfo { get; set; }
        public IEnumerable<Quadrocopter> Quadrocopters { get; set; }
    }
}