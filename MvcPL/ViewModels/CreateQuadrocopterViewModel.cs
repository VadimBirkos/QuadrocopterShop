﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using BLL.Interface.Entities;

namespace MvcPL.ViewModels
{
    public class CreateQuadrocopterViewModel
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите модель квадрокоптера")]
        [Display(Name = "Модель")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Введите модель квадрокоптера")]
        [Display(Name = "Цена")]
        public double Cost { get; set; }

        public HttpPostedFileBase UploadedPreviewImage { get; set; }

        [Display(Name = "Артикль")]
        public int Article { get; set; }

        [Display(Name = "Дальность управления, метры")]
        public int ControlDistance { get; set; }

        [Display(Name = "Наличие камеры")]
        public int CameraPresenseId { get; set; }

        [Display(Name = "Брэнд")]
        public int BrandId { get; set; }

        [Display(Name = "Страна сборки")]
        public int MadeCountryId { get; set; }
    }
}