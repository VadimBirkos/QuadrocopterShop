﻿function addToBusket(name, id) {
    $.get(
        "/Basket/AddToBusket/",
        {
            userName: name,
            quadrocopterId: id
        },
        function () {
            $.get(
                "/Basket/GetCountOfItems/",
                function (data) {
                    CountOfBasket(data);
                }
            );
        });
}

function CountOfBasket(count) {
    var spanEl = document.getElementById("countOfItems");
    spanEl.innerText = count;
}

function filterQuadrocopters(page) {
    var checkBoxes = document.getElementsByClassName("checkedFilter");
    var filterParametrs = { param: [] };
    for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            var model = {
                "Value": checkBoxes[i].value,
                "Critery": checkBoxes[i].id
            };
            filterParametrs.param.push(model);
        }
    }

    var countSelect = document.getElementById("countItems");
    let pageSize = countSelect.value;
    if (filterParametrs.param.length > 0) {
        $.post('/Quadrocopter/FilterQuadrocopers',
            { Filter: filterParametrs.param, page: page, pageSize: pageSize },
            function (data) {
                onAjaxSuccess(data);
            });

    } else {
        $("#quadrocoptersList").load(`/Quadrocopter/Index/?page=${page}&pageSize=${pageSize}`);
    }
}

function onAjaxSuccess(data) {
    var quadHtml = document.getElementById("quadrocoptersList");
    quadHtml.innerHTML = data;
}

function loadList(page) {
    if (!filterQuadrocopters(page)) {
        var countSelect = document.getElementById("countItems");
        let pageSize = countSelect.value;
        debugger;
        $("#quadrocoptersList").load(`/Quadrocopter/Index/?page=${page}&pageSize=${pageSize}`);
    }
}

function changeCountOfElementsOnView() {
    if (!filterQuadrocopters(1)) {
        var countSelect = document.getElementById("countItems");
        let pageSize = countSelect.value;
        $("#quadrocoptersList").load(`/Quadrocopter/Index/?pageSize=${pageSize}`);
    }
}

function clearFilters() {
    const checkBoxes = document.getElementsByClassName("checkedFilter");
    for (let i = 0; i < checkBoxes.length; i++) {
        checkBoxes[i].checked = false;
    }
    loadList(1);
}