﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using CommonInterface;
using DAL.Interface;

namespace DAL.Implementation
{
    public class BaseRepository<TEntity>:IRepository<TEntity> where TEntity: class, IEntity
    {
        private readonly DbContext _context;

        public BaseRepository(DbContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));
            _context = context;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return _context.Set<TEntity>().AsEnumerable();
        }

        public virtual TEntity GetById(int key)
        {
            return _context.Set<TEntity>()
                            .FirstOrDefault(entity => entity.Id == key);
        }

        public virtual void Create(TEntity entity)
        {
            var entry = _context.Entry<TEntity>(entity);
            _context.Set<TEntity>().Add(entry.Entity);
        }

        public virtual void Delete(TEntity entity)
        {
            var dbEntity = _context.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            if (dbEntity != null) _context.Set<TEntity>().Remove(dbEntity);
        }

        public void Update(TEntity entity)
        {
            var dbEntity = _context.Set<TEntity>().FirstOrDefault(e => e.Id == entity.Id);
            if (dbEntity != null) _context.Entry((object)dbEntity).CurrentValues.SetValues(entity);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _context.Set<TEntity>().Where(predicate).AsEnumerable();
        }
    }
}
