﻿using DAL.Interface.DalModels;
using System.Data.Entity;

namespace DAL.Implementation
{
    public class EntityModel : DbContext
    {
        public EntityModel() : base("QuadrocopterShopEntity")
        {
        }

        public virtual DbSet<DalUser> Users { get; set; }
        public virtual DbSet<DalRole> Roles { get; set; }
        public virtual DbSet<DalQuadrocopter> Quadrocopters { get; set; }
        public virtual DbSet<DalBrand> Brands { get; set; }
        public virtual DbSet<DalQuadrocopterBasket> QuadrocopterBaskets { get; set; }
        public virtual DbSet<DalBasket> Baskets { get; set; }
        public virtual DbSet<DalCameraPresense> CameraPresenses { get; set; }
        public virtual DbSet<DalCountry> Countries { get; set; }
    }
}