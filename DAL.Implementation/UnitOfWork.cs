﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DAL.Interface;

namespace DAL.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        public UnitOfWork(DbContext context)
        {
            _context = context;
        }

        public DbContext Context
        {
            get { return _context; }
        }

        public void Commit()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                var a = ex.InnerException;
            }
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
