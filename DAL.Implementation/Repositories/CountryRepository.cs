﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class CountryRepository:BaseRepository<DalCountry>, ICountryRepository
    {
        public CountryRepository(DbContext context) : base(context)
        {
        }
    }
}