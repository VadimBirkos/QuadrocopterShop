﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class QuadrocopterRepository:BaseRepository<DalQuadrocopter>, IQuadrocopterRepository
    {
        public QuadrocopterRepository(DbContext context) : base(context)
        {
        }
    }
}