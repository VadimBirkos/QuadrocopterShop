﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class UserRepository:BaseRepository<DalUser>,IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
            
        }
    }
}