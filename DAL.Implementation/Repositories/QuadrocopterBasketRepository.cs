﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class QuadrocopterBasketRepository:BaseRepository<DalQuadrocopterBasket>,IQuadrocopterBasketRepository
    {
        public QuadrocopterBasketRepository(DbContext context) : base(context)
        {
        }

        public IEnumerable<DalQuadrocopterBasket> GetBasketQuadrokopters(int basketId)
        {
            return Find(item => item.BasketId == basketId);
        }
    }
}
