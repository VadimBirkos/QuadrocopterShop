﻿using System.Data.Entity;
using System.Linq;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class BasketRepository:BaseRepository<DalBasket>, IBasketRepository
    {
        public BasketRepository(DbContext context) : base(context)
        {
        }

        public DalBasket GetByUserId(int userId)
        {
            return base.Find(basket => basket.UserId == userId).FirstOrDefault();
        }
    }
}
