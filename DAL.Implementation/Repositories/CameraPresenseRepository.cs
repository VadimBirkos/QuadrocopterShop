﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class CameraPresenseRepository:BaseRepository<DalCameraPresense>,ICameraPresenseRepository
    {
        public CameraPresenseRepository(DbContext context) : base(context)
        {
        }
    }
}
