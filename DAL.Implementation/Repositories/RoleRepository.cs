﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class RoleRepository:BaseRepository<DalRole>,IRoleRepository
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }
    }
}