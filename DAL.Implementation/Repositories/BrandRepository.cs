﻿using System.Data.Entity;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace DAL.Implementation.Repositories
{
    public class BrandRepository:BaseRepository<DalBrand>, IBrandRepository
    {
        public BrandRepository(DbContext context) : base(context)
        {
        }
    }
}