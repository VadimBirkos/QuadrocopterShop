﻿using CommonInterface;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public interface IEntityMapper<TBllEntity, TDalEntity>
         where TBllEntity : IEntity
         where TDalEntity : IEntity
    {
        TBllEntity GetBllEntity(TDalEntity dalEntity);
        TDalEntity GetDalEntity(TBllEntity bllEntity);
    }
}