﻿using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class BasketMapper:IEntityMapper<Basket, DalBasket>
    {
        public Basket GetBllEntity(DalBasket dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new Basket
            {
                Id = dalEntity.Id
            };
        }

        public DalBasket GetDalEntity(Basket bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalBasket
            {
                Id = bllEntity.Id,
                UserId = bllEntity.User.Id
            };
        }
    }
}
