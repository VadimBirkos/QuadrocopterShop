﻿using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class CameraPresenseMapper:IEntityMapper<CameraPresense, DalCameraPresense>
    {
        public CameraPresense GetBllEntity(DalCameraPresense dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }
            return new CameraPresense
            {
                Id = dalEntity.Id,
                Type = dalEntity.Type   
            };
        }

        public DalCameraPresense GetDalEntity(CameraPresense bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalCameraPresense
            {
                Id = bllEntity.Id,
                Type = bllEntity.Type
            };
        }
    }
}