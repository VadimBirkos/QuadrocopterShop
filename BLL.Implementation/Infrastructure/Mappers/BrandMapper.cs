﻿using System;
using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class BrandMapper:IEntityMapper<Brand, DalBrand>
    {
        public Brand GetBllEntity(DalBrand dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new Brand
            {
                Id = dalEntity.Id,
                Description = dalEntity.Description,
                Name = dalEntity.Name
            };
        }

        public DalBrand GetDalEntity(Brand bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalBrand
            {
                Id = bllEntity.Id,
                CountryId = bllEntity.Country.Id,
                Description = bllEntity.Description,
                Name = bllEntity.Name
            };
        }
    }
}