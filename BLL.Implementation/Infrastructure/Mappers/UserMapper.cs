﻿using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class UserMapper:IEntityMapper<User, DalUser>
    {
        public User GetBllEntity(DalUser dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new User()
            {
                Id = dalEntity.Id,
                AvatarPath = dalEntity.AvatarPath,
                Created = dalEntity.Created,
                Login = dalEntity.Login,
                Password = dalEntity.HashPassword
            };
        }

        public DalUser GetDalEntity(User bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
                
            }

            return new DalUser()
            {
                Id = bllEntity.Id,
                AvatarPath = bllEntity.AvatarPath,
                Created = bllEntity.Created,
                Login = bllEntity.Login,
                HashPassword = bllEntity.Password,
                RoleId = bllEntity.Role.Id,
                BasketId = bllEntity.Basket?.Id ?? 0
            };
        }
    }
}
