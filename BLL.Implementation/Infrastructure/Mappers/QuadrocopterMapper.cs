﻿using System;
using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class QuadrocopterMapper:IEntityMapper<Quadrocopter, DalQuadrocopter>
    {
        public Quadrocopter GetBllEntity(DalQuadrocopter dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new Quadrocopter
            {
                Id = dalEntity.Id,
                Article = dalEntity.Article,
                ControlDistance = dalEntity.ControlDistance,
                Cost = dalEntity.Cost,
                Description = dalEntity.Description,
                ImagePath = dalEntity.ImagePath,
                Name = dalEntity.Name
            };
        }

        public DalQuadrocopter GetDalEntity(Quadrocopter bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalQuadrocopter
            {
                Id = bllEntity.Id,
                Article = bllEntity.Article,
                BrandId = bllEntity.Brand.Id,
                CameraPresenseId = bllEntity.CameraPresense.Id,
                ControlDistance = bllEntity.ControlDistance,
                Cost = bllEntity.Cost,
                Description = bllEntity.Description,
                ImagePath = bllEntity.ImagePath,
                MadeCountryId = bllEntity.MadeCountry.Id,
                Name = bllEntity.Name

            };
        }
    }
}