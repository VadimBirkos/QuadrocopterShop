﻿using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class CountryMapper:IEntityMapper<Country, DalCountry>
    {
        public Country GetBllEntity(DalCountry dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new Country
            {
                Id = dalEntity.Id,
                Name = dalEntity.Name
            };
        }

        public DalCountry GetDalEntity(Country bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalCountry
            {
                Id = bllEntity.Id,
                Name = bllEntity.Name
            };
        }
    }
}