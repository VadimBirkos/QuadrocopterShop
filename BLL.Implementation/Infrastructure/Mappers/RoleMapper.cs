﻿using System;
using BLL.Interface.Entities;
using DAL.Interface.DalModels;

namespace BLL.Implementation.Infrastructure.Mappers
{
    public class RoleMapper:IEntityMapper<Role, DalRole>
    {
        public Role GetBllEntity(DalRole dalEntity)
        {
            if (dalEntity == null)
            {
                return null;
            }

            return new Role()
            {
                Id = dalEntity.Id,
                Title = dalEntity.Title
            };
        }

        public DalRole GetDalEntity(Role bllEntity)
        {
            if (bllEntity == null)
            {
                return null;
            }

            return new DalRole()
            {
                Id = bllEntity.Id,
                Title = bllEntity.Title
            };
        }
    }
}
