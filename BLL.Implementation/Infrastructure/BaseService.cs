﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BLL.Implementation.Infrastructure.Mappers;
using BLL.Interface;
using CommonInterface;
using DAL.Interface;

namespace BLL.Implementation.Infrastructure
{
    public abstract class BaseService<TEntity, TDalEntity, TRepository, TEntityMapper>
        : IService<TEntity>
        where TEntity : class, IEntity
        where TDalEntity : class, IEntity
        where TRepository : IRepository<TDalEntity>
        where TEntityMapper : IEntityMapper<TEntity, TDalEntity>, new()
    {
        protected readonly TRepository Repository;
        protected readonly IUnitOfWork _uow;
        protected readonly TEntityMapper Mapper = new TEntityMapper();

        protected BaseService(TRepository repository, IUnitOfWork uow)
        {
            Repository = repository;
            _uow = uow;
        }

        public virtual IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll().Select(Mapper.GetBllEntity).ToList();
            //dbcontext
        }

        public virtual TEntity GetById(int key)
        {
            var dalEntity = Repository.GetById(key);
            return Mapper.GetBllEntity(dalEntity);
            //dbcontext
        }

        public virtual IEnumerable<TEntity> GetByPredicate(Expression<Func<TEntity, bool>> predicate)
        {
            var dalEntities = GetAll().Where(predicate.Compile()).AsEnumerable();
            return dalEntities;
        }

        public virtual void Create(TEntity entity)
        {
            var dalEntity = Mapper.GetDalEntity(entity);
            Repository.Create(dalEntity);
            _uow.Commit();
            //_uow.Dispose();
        }

        public virtual void Delete(TEntity entity)
        {
            var dalEntity = Mapper.GetDalEntity(entity);
            Repository.Delete(dalEntity);
            _uow.Commit();
            //_uow.Dispose();
        }

        public virtual void Update(TEntity entity)
        {
            var dalEntity = Mapper.GetDalEntity(entity);
            Repository.Update(dalEntity);
            _uow.Commit();
            //_uow.Dispose();
        }
    }
}
