﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BLL.Implementation.Infrastructure;
using BLL.Implementation.Infrastructure.Mappers;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DAL.Interface;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace BLL.Implementation.Services
{
    public class QuadrocopterService :
        BaseService<Quadrocopter, DalQuadrocopter, IQuadrocopterRepository, QuadrocopterMapper>, IQuadrocopterService
    {
        private IEntityMapper<Country, DalCountry> _countryMapper = new CountryMapper();
        private IEntityMapper<Brand, DalBrand> _brandMapper = new BrandMapper();
        private IEntityMapper<CameraPresense, DalCameraPresense> _cameraMapper = new CameraPresenseMapper();
        private ICountryRepository _country;
        private IBrandRepository _brand;
        private ICameraPresenseRepository _camera;

        public QuadrocopterService(IQuadrocopterRepository repository, IUnitOfWork uow,

            ICountryRepository country,
            IBrandRepository brand, ICameraPresenseRepository camera) : base(repository, uow)
        {
            _country = country;
            _brand = brand;
            _camera = camera;
        }

        public override Quadrocopter GetById(int key)
        {
            var dalEntity = Repository.GetById(key);
            if (dalEntity == null)
            {
                return null;
            }
            var bllEntity = Mapper.GetBllEntity(dalEntity);

            bllEntity.CameraPresense = _cameraMapper.GetBllEntity(_camera.GetById(dalEntity.CameraPresenseId));
            bllEntity.Brand = _brandMapper.GetBllEntity(_brand.GetById(dalEntity.BrandId));
            bllEntity.MadeCountry = _countryMapper.GetBllEntity(_country.GetById(dalEntity.MadeCountryId));
            return bllEntity;
        }

        public override IEnumerable<Quadrocopter> GetAll()
        {
            var  dalquadrocopters = Repository.GetAll().ToList();
            var result = new List<Quadrocopter>();
            foreach (var quadrocopter in dalquadrocopters)
            {
                result.Add(Mapper.GetBllEntity(quadrocopter));
                result.Last().CameraPresense = _cameraMapper.GetBllEntity(_camera.GetById(quadrocopter.CameraPresenseId));
                result.Last().Brand = _brandMapper.GetBllEntity(_brand.GetById(quadrocopter.BrandId));
                result.Last().MadeCountry = _countryMapper.GetBllEntity(_country.GetById(quadrocopter.MadeCountryId));
            }
            return result;
        }
    }
}
