﻿using System;
using System.Collections.Generic;
using System.Linq;
using BLL.Implementation.Infrastructure;
using BLL.Implementation.Infrastructure.Mappers;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DAL.Interface;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace BLL.Implementation.Services
{
    public class UserService:BaseService<User,DalUser,IUserRepository,UserMapper>,IUserService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IBasketRepository _basketRepository;
        private readonly IEntityMapper<Basket, DalBasket> _basketMapper = new BasketMapper();
        private readonly IEntityMapper<Role, DalRole> _roleMapper = new RoleMapper();

        public UserService(IUserRepository repository, IUnitOfWork uow, IRoleRepository roleRepository, IBasketRepository basketRepository) : base(repository, uow)
        {
            _roleRepository = roleRepository;
            _basketRepository = basketRepository;
        }

        public override void Create(User user)
        {
            var userInDb = GetByLogin(user.Login);
            if (userInDb != null)
                throw new InvalidOperationException("User already in database");
            user.Role = _roleMapper.GetBllEntity(_roleRepository.GetById(user.Role.Id));
            base.Create(user);
            var newUser = GetByLogin(user.Login);
            _basketRepository.Create(new DalBasket
            {
                UserId = newUser.Id
            });
            _uow.Commit();
            var basket = _basketRepository.GetByUserId(newUser.Id);
            newUser.Basket = new Basket
            {
                Id = basket.Id,
                User = newUser
            };
            Update(newUser);
        }

        public override User GetById(int key)
        {
            var dalEntity = Repository.GetById(key);
            if (dalEntity == null)
            {
                return null;
            }
            var bllEntity = Mapper.GetBllEntity(dalEntity);
            bllEntity.Role = GetBllRoleById(dalEntity.RoleId);
            bllEntity.Basket = GetBllBasketById(dalEntity.BasketId);
            return bllEntity;
        }

        public override IEnumerable<User> GetAll()
        {
            return Repository.GetAll().Select(user =>
            {
                var result = Mapper.GetBllEntity(user);
                result.Role = GetBllRoleById(user.RoleId);
                result.Basket = GetBllBasketById(user.BasketId);
                return result;
            }).ToList();
        }

        public virtual User GetByLogin(string login)
        {
            var userInDb = Repository
                           .Find(u => u.Login == login)
                           .FirstOrDefault();

            if (userInDb == null) return null;
            var bllEntity = Mapper.GetBllEntity(userInDb);
            bllEntity.Role = GetBllRoleById(userInDb.RoleId);
            bllEntity.Basket = GetBllBasketById(userInDb.BasketId);
            return bllEntity;
        }

        private Role GetBllRoleById(int roleId)
        {
            return _roleMapper.GetBllEntity(_roleRepository.GetById(roleId));
        }

        private Basket GetBllBasketById(int basketId)
        {
            return _basketMapper.GetBllEntity(_basketRepository.GetById(basketId));
        }
    }
}
