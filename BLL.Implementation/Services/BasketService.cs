﻿using System.Collections.Generic;
using System.Linq;
using BLL.Implementation.Infrastructure;
using BLL.Implementation.Infrastructure.Mappers;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DAL.Interface;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace BLL.Implementation.Services
{
    public class BasketService : BaseService<Basket, DalBasket, IBasketRepository, BasketMapper>, IBasketService
    {
        private readonly IQuadrocopterBasketRepository _quadroBasket;
        private readonly IQuadrocopterService _quadrocopterService;
        private readonly IUserService _userService;


        public BasketService(IBasketRepository repository, IUnitOfWork uow,
            IQuadrocopterBasketRepository quadroBasket, IQuadrocopterService quadrocopterService,
            IUserService userService) : base(repository, uow)
        {
            _quadroBasket = quadroBasket;
            _quadrocopterService = quadrocopterService;
            _userService = userService;
        }

        public override Basket GetById(int key)
        {
            var dalBasket = Repository.GetById(key);
            var basket = base.GetById(key);
            basket.User = _userService.GetById(dalBasket.UserId);
            basket.Quadrocopters = GetBasketQuadrocopters(key);
            return basket;
        }

        public IEnumerable<Quadrocopter> GetBasketQuadrocopters(int basketid)
        {
            var basketQuadro = _quadroBasket.GetBasketQuadrokopters(basketid).ToList();
            return basketQuadro.Select(item => _quadrocopterService.GetById(item.QuadrocopterId)).AsEnumerable();
        }

        public void AddInBusket(int basketId, params Quadrocopter[] quadrocopters)
        {
            if (quadrocopters == null)
            {
                return;
            }
            foreach (var quadrocopter in quadrocopters)
            {
                _quadroBasket.Create(new DalQuadrocopterBasket {BasketId = basketId, QuadrocopterId = quadrocopter.Id});
            }
            _uow.Commit();
        }


        public override void Delete(Basket entity)
        {
            var quadBaskets = _quadroBasket.GetAll().Where(item => item.BasketId == entity.Id).ToList();
            foreach (var quadBasket in quadBaskets)
            {
                _quadroBasket.Delete(quadBasket);
            }
            _uow.Commit();
            base.Delete(entity);
        }

        public void DeleteQuadrocopter(int basketId, int productId)
        {
            var quadroBasket =
                _quadroBasket.Find(item => item.BasketId == basketId && item.QuadrocopterId == productId)
                    .FirstOrDefault();
            _quadroBasket.Delete(quadroBasket);
            _uow.Commit();
        }

        public IEnumerable<QuadrocopterBasket> GetQuadroBasket(int basketId)
        {
            var dalBasket = _quadroBasket.GetAll().Where(item => item.BasketId == basketId);
            return dalBasket.Select(item => new QuadrocopterBasket()
            {
                BasketId = item.BasketId, Id = item.Id, QuadrocopterId = item.QuadrocopterId
            }).ToList();
        }
    }
}