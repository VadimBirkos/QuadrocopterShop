﻿using BLL.Implementation.Infrastructure;
using BLL.Implementation.Infrastructure.Mappers;
using BLL.Interface.Entities;
using BLL.Interface.Services;
using DAL.Interface;
using DAL.Interface.DalModels;
using DAL.Interface.Repositories;

namespace BLL.Implementation.Services
{
    public class CountryService:BaseService<Country, DalCountry, ICountryRepository, CountryMapper>, ICountryService
    {
        public CountryService(ICountryRepository repository, IUnitOfWork uow) : base(repository, uow)
        {
        }
    }
}
