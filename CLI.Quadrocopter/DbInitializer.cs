﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DAL.Implementation;
using DAL.Interface.DalModels;

namespace CLI.Quadrocopter
{
    public class DbInitializer : CreateDatabaseIfNotExists<EntityModel>
    {
        protected override void Seed(EntityModel entityModel)
        {
            InitializeCountry(entityModel);
            base.Seed(entityModel);

        }

        private void InitializeCountry(EntityModel entityModel)
        {
            entityModel.Countries.AddRange(new List<DalCountry>()
            {
                new DalCountry()
                {
                    Name = "Австралия"
                },
                new DalCountry()
                {
                    Name = "Америка"
                },
                new DalCountry()
                {
                    Name = "Россия"
                },
                new DalCountry()
                {
                    Name = "Турция"
                },
                new DalCountry()
                {
                    Name = "Тайвань"
                },
                new DalCountry()
                {
                    Name = "Китай"
                },
                new DalCountry()
                {
                    Name = "Япония"
                },
                 new DalCountry()
                {
                    Name = "Украина"
                }
            });

            entityModel.Roles.AddRange(new List<DalRole>
            {
                new DalRole()
                {
                    Title = "Admin"
                },

                new DalRole()
                {
                    Title = "User"
                }
            });


            entityModel.CameraPresenses.AddRange(new List<DalCameraPresense>
            {
                new DalCameraPresense()
                {
                    Type = "Есть"
                },

                new DalCameraPresense()
                {
                    Type = "Отсутствует"
                },

                new DalCameraPresense()
                {
                    Type = "Можно установить"
                }
            });

            entityModel.Brands.AddRange(new List<DalBrand>()
            {
                new DalBrand()
                {
                    Name = "DJI",
                    CountryId = 6,
                    Description = @"Da-Jiang Innovations Science and Technology Co., Ltd. 14th Floor, West Wing, 
                    Skyworth Semiconductor Design Building,
                    No.18 Gaoxin South 4th Ave,
                    Nanshan
            
                    District,
                    Shenzhen 518057,
                    China."
                },

                new DalBrand()
                {
                    Name = "Syma",
                    CountryId = 6,
                    Description =
                        "Syma Toys Co.Ltd. Laimei Industrial park Chenghai District Shantou City Guangdong China."
                },

                new DalBrand()
                {
                    Name = "WLtoys",
                    CountryId = 6,
                    Description = @"Shantou Chenghai WL Toys Industrial Co., Ltd. China, Guangdong, 515800, Fengxin
                    Industry Park,
                    Fengxin 2th Road,
                    Chenghai District,
                    Shantou City."
                },

                new DalBrand()
                {
                    Name = "MJX",
                    CountryId = 5,
                    Description = @"Meajaixin Toys Co.,Ltd. Room 1110, 11F, Concordia Plaza, 1 Science Museum Road, s
                    Tsimshatsui,
                    Kowloon,
                    HongKong."
                },

                new DalBrand()
                {
                    Name = "UDI",
                    CountryId = 3,
                    Description =
                        "UDIRCTOYS INDUSTRY CO.,LTD. Dengfeng Industrial Zone,Chenghai District,Shantou City,Guangdong,China."
                }
            });

            entityModel.Quadrocopters.AddRange(new List<DalQuadrocopter>()
            {
                new DalQuadrocopter()
                {
                    Name = "X8HG",
                    Description =
                        @"Syma X8HG – это усовершенствованная версия модели Syma X8G. Выпускается в красном цвете и имеет 8 MP камеру, 
                    которая позволяет делать отличные фото и снимать видео.Главным улучшением данной модели стал
                    встроенный барометр,
                    который
                    позволяет квадрокоптеру зависать на одной высоте,
                    значительно упрощая пилотирование.",
            
                    Cost = 150,
                    ImagePath = null,
                    Article = 12345434,
                    ControlDistance = 100,
                    CameraPresenseId = 3,
                    BrandId = 1,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "X54HW",
                    Description =
                        "Квадрокоптер с камерой и барометром. Видео с камеры транслируется на смартфон в режиме реального времени.",
                    Cost = 99,
                    ImagePath = null,
                    Article = 87654321,
                    ControlDistance = 30,
                    CameraPresenseId = 1,
                    BrandId = 1,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "Phantom 3 Professional",
                    Description =
                        @"Рассчитан на опытных пилотов и операторов аэросъемки. Обладает камерой, способной вести запись видео в формате 4K 
                            (для
                    Phantom 3 Professional).Для управления в качестве видоискателя можно подключить устройства под
                    управлением iOS 8.0 + или Android 4.1.2 +.",

                    Cost = 400,
                    ImagePath = null,
                    Article = 12345678,
                    ControlDistance = 6000,
                    CameraPresenseId = 1,
                    BrandId = 2,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "Matrice 100",
                    Description =
                        @"Наличие специальных отсеков для модулей расширения. Модернизированный контроллер полета N1. Встроенный видеолинк для 
                    аэрофотосъемки DJI Lightbridge.Усовершенствованная система моторов и световых контроллеров скорости.
                    Корпус изготовлен из
                    прочного и легкого углеродного волокна.Усовершенствованный пульт управления.Улучшенный модуль GPS",
                    Cost = 300,
                    ImagePath = null,
                    Article = 18273645,
                    ControlDistance = 2000,
                    CameraPresenseId = 2,
                    BrandId = 2,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "Inspire 1",
                    Description =
                        @"Квадрокоптер профессионального уровня для профессиональной фото и видеосъемки. Может снимать видео в разрешении 4К, 
                    причем есть возможность одновременно транслировать HD - запись сразу на несколько принимающих
                    устройств.В 4К - режиме камера
                    позволяет записывать ролики с частотой кадров 24 - 30 в секунду,
                    в Full HD — 24 - 60 FPS.Снимки можно делать в качестве 12 МП,
                    при
                    этом камера оснащена 1 / 2.3 - дюймовым CMOS - сенсором и линзой с углом 94 градуса.Скорость DJI
                    Inspire 1 составляет 80 км / ч.",

                    Cost = 350,
                    ImagePath = null,
                    Article = 12345670,
                    ControlDistance = 2000,
                    CameraPresenseId = 1,
                    BrandId = 2,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "Phantom 3 Advanced",
                    Description =
                        @"Рассчитан на опытных пилотов и операторов аэросъемки. Обладает камерой, способной вести запись видео.
                    Для управления в качестве видоискателя можно подключить устройства под управлением iOS 8.0 + или
                    Android 4.1.2 +.",

                    Cost = 300,
                    ImagePath = null,
                    Article = 12345600,
                    ControlDistance = 2000,
                    CameraPresenseId = 1,
                    BrandId = 2,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "X400",
                    Description =
                        @"Квадрокоптер средних размеров, предназначенный для полетов по FPV. К этому квадрокоптеру можно докупить 
                    специальную камеру,
                    которая будет транслировать видео прямо на Ваш смартфон!Теперь FPV доступно каждому!",

                    Cost = 50,
                    ImagePath = null,
                    Article = 12345000,
                    ControlDistance = 100,
                    CameraPresenseId = 2,
                    BrandId = 4,
                    MadeCountryId = 5
                },

                new DalQuadrocopter()
                {
                    Name = "U842-1",
                    Description = "Квадрокоптер средних рамезров с многофункциональным пультом управления.",
                    Cost = 200,
                    ImagePath = null,
                    Article = 00123456,
                    ControlDistance = 50,
                    CameraPresenseId = 3,
                    BrandId = 5,
                    MadeCountryId = 3
                },

                new DalQuadrocopter()
                {
                    Name = "V262",
                    Description =
                        @"WLtoys V262 – это самый большой квадрокоптер в линейке WLtoys, обладающий отличными лётными качествами. 
                    Благодаря 6 осевому гироскопу модель очень стабильна и устойчива даже в ветреную погоду.Оснащена
                    аппаратурой 2,
                    4 ГГц с возможностью регулировки расходов двигателя и дальностью до 130 метров.Возможность
                    3D переворотов и подключения камеры типа GoPro или аналогичной.",
                    Cost = 95,
                    ImagePath = null,
                    Article = 01234567,
                    ControlDistance = 130,
                    CameraPresenseId = 2,
                    BrandId = 3,
                    MadeCountryId = 6
                },

                new DalQuadrocopter()
                {
                    Name = "Phantom 3 Standard",
                    Description =
                        @"Standard отличается от старших версий тем, что у него такой же пульт управления, как на втором поколении. 
                    Он намного проще по всем характеристикам,
                    подойдет только начинающим «летчикам» и тем,
                    кому фотография
                    не очень нужна(нет контроля за камерой с помощью кнопок).Для управления в качестве видоискателя
                    можно подключить устройства
                    под управлением iOS 8.0 + или Android 4.1.2 +.",
                    Cost = 1,
                    ImagePath = null,
                    Article = 00123450,
                    ControlDistance = 6000,
                    CameraPresenseId = 3,
                    BrandId = 2,
                    MadeCountryId = 6
                }
            })
            ;


            entityModel.Baskets.AddRange(new List<DalBasket>()
            {
                new DalBasket()
                {
                    UserId = 1
                },

                new DalBasket()
                {
                    UserId = 2

                },

                new DalBasket()
                {
                    UserId = 3

                },

                new DalBasket()
                {
                    UserId = 4

                },

                new DalBasket()
                {
                    UserId = 5

                },

                new DalBasket()
                {
                    UserId = 6

                },

                new DalBasket()
                {
                    UserId = 7

                }
            });

            entityModel.QuadrocopterBaskets.AddRange(new List<DalQuadrocopterBasket>()
            {
                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 1,
                    BasketId = 1
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 2,
                    BasketId = 1
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 2,
                    BasketId = 2
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 2,
                    BasketId = 3
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 3,
                    BasketId = 3
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 3,
                    BasketId = 4
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 4,
                    BasketId = 5
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 5,
                    BasketId = 6
                },

                new DalQuadrocopterBasket()
                {
                    QuadrocopterId = 6,
                    BasketId = 7
                }
            });


            entityModel.Users.AddRange(new List<DalUser>()
            {
                new DalUser()
                {
                    Login = "sam_smith",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 1
                },

                new DalUser()
                {
                    Login = "john_smith",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 2
                },

                new DalUser()
                {
                    Login = "elen_smith",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 3
                },

                new DalUser()
                {
                    Login = "ivan_ivanov",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 4
                },

                new DalUser()
                {
                    Login = "olga_ivanova",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 5
                },

                new DalUser()
                {
                    Login = "alex_sidorov",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 6
                },

                new DalUser()
                {
                    Login = "veronika_sidorova",
                    HashPassword = "",
                    RoleId = 2,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 7
                },

                new DalUser()
                {
                    Login = "admin",
                    HashPassword = "",
                    RoleId = 1,
                    AvatarPath = null,
                    Created = DateTime.Now,
                    BasketId = 8 
                }
            });
        }
    }
}
