﻿using System;
using System.Data.Entity;
using DAL.Implementation;

namespace CLI.Quadrocopter
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new EntityModel();
            Console.WriteLine("Input your command: 'initialize' for initialize project ");
            if (Console.ReadLine() == "initialize")
            {
                Database.SetInitializer(new DbInitializer());
                context.Database.Initialize(true);
            }
        }
    }
}
