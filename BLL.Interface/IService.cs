﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CommonInterface;

namespace BLL.Interface
{
    public interface IService<TEntity> where TEntity:IEntity
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int key);
        IEnumerable<TEntity> GetByPredicate(Expression<Func<TEntity, bool>> predicate);
        void Create(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
