﻿using CommonInterface;

namespace BLL.Interface.Entities
{
    public class QuadrocopterBasket:IEntity
    {
        public int Id { get; set; }
        public int BasketId { get; set; }
        public int QuadrocopterId { get; set; }
    }
}