﻿using System.ComponentModel.DataAnnotations;
using CommonInterface;

namespace BLL.Interface.Entities
{
    public class Quadrocopter:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public string ImagePath { get; set; }
        public int Article { get; set; }
        public int ControlDistance { get; set; }
        public CameraPresense CameraPresense { get; set; }
        public Brand Brand { get; set; }
        public Country MadeCountry { get; set; }
    }
}
