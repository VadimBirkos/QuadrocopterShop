﻿using CommonInterface;

namespace BLL.Interface.Entities
{
    public class Brand : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }
        public string Description { get; set; }
    }
}