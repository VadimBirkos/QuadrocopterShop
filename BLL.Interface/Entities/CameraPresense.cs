﻿using CommonInterface;

namespace BLL.Interface.Entities
{
    public class CameraPresense : IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}