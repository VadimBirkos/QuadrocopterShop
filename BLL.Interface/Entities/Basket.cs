﻿using System.Collections.Generic;
using CommonInterface;

namespace BLL.Interface.Entities
{
    public class Basket:IEntity
    {
        public int Id { get; set; }
        public User User { get; set; }
        public IEnumerable<Quadrocopter> Quadrocopters { get; set; }
    }
}
