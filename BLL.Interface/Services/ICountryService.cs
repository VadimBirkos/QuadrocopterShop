﻿using BLL.Interface.Entities;

namespace BLL.Interface.Services
{
    public interface ICountryService:IService<Country>
    {
    }
}
