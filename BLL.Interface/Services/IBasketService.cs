﻿using System.Collections.Generic;
using BLL.Interface.Entities;

namespace BLL.Interface.Services
{
    public interface IBasketService:IService<Basket>
    {
        IEnumerable<Quadrocopter> GetBasketQuadrocopters(int basketid);
        void AddInBusket(int basketId, params Quadrocopter[] quadrocopters);
        void DeleteQuadrocopter(int basketId, int productId);
        IEnumerable<QuadrocopterBasket> GetQuadroBasket(int basketId);
    }
}