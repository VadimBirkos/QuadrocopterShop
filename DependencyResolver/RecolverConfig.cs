﻿using System.Data.Entity;
using BLL.Implementation.Services;
using BLL.Interface.Services;
using DAL.Implementation;
using DAL.Implementation.Repositories;
using DAL.Interface;
using DAL.Interface.Repositories;
using Ninject;
using Ninject.Web.Common;

namespace DependencyResolver
{
    public static class RecolverConfig
    {
        public static void ConfigureWeb(this IKernel kernel)
        {
            Configure(kernel);
        }

        private static void Configure(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            kernel.Bind<DbContext>().To<EntityModel>().InRequestScope();

            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IRoleRepository>().To<RoleRepository>();
            kernel.Bind<IBasketRepository>().To<BasketRepository>();
            kernel.Bind<IBrandRepository>().To<BrandRepository>();
            kernel.Bind<ICameraPresenseRepository>().To<CameraPresenseRepository>();
            kernel.Bind<ICountryRepository>().To<CountryRepository>();
            kernel.Bind<IQuadrocopterBasketRepository>().To<QuadrocopterBasketRepository>();
            kernel.Bind<IQuadrocopterRepository>().To<QuadrocopterRepository>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IBasketService>().To<BasketService>();
            kernel.Bind<IQuadrocopterService>().To<QuadrocopterService>();
            kernel.Bind<IBrandService>().To<BrandService>();
            kernel.Bind<ICountryService>().To<CountryService>();
            kernel.Bind<ICameraPresenceService>().To<CameraPresenceService>();
        }
    }
}
