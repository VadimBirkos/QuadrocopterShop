﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface ICameraPresenseRepository:IRepository<DalCameraPresense>
    {
    }
}
