﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IUserRepository:IRepository<DalUser>
    {
    }
}
