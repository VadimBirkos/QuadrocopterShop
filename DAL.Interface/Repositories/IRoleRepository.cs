﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IRoleRepository:IRepository<DalRole>
    {
    }
}
