﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface ICountryRepository:IRepository<DalCountry>
    {
    }
}
