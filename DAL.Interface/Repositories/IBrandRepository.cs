﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IBrandRepository:IRepository<DalBrand>
    {
    }
}
