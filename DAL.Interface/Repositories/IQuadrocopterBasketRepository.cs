﻿using System.Collections.Generic;
using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IQuadrocopterBasketRepository:IRepository<DalQuadrocopterBasket>
    {
        IEnumerable<DalQuadrocopterBasket> GetBasketQuadrokopters(int basketId);
    }
}
