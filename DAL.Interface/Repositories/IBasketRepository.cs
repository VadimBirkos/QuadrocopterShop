﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IBasketRepository:IRepository<DalBasket>
    {
        DalBasket GetByUserId(int userId);
    }
}
