﻿using DAL.Interface.DalModels;

namespace DAL.Interface.Repositories
{
    public interface IQuadrocopterRepository:IRepository<DalQuadrocopter>
    {
    }
}
