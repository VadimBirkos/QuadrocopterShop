﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Countries")]
    public class DalCountry:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
