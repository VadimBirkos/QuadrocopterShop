﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Brands")]
    public class DalBrand:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public string Description { get; set; }
    }
}
