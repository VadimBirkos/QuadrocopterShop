﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Users")]
    public class DalUser:IEntity
    {

        public int Id { get; set; }

        public string Login { get; set; }

        public string HashPassword { get; set; }

        public int RoleId { get; set; }

        public string AvatarPath { get; set; }

        public DateTime Created { get; set; }

        public int BasketId { get; set; }
    }
}
