﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Baskets")]
    public class DalBasket:IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
    }
}
