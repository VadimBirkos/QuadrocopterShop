﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Roles")]
    public class DalRole:IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
