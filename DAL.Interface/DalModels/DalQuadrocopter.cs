﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Quadrocopters")]
    public class DalQuadrocopter:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Cost { get; set; }
        public string ImagePath { get; set; }
        public int Article { get; set; }
        public int ControlDistance { get; set; }
        public int CameraPresenseId { get; set; }
        public int BrandId { get; set; }
        public int MadeCountryId { get; set; }
    }
}
