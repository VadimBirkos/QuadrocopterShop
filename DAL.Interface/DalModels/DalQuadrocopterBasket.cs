﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("Quadrocopters_Baskets")]
    public class DalQuadrocopterBasket:IEntity
    {
        public int Id { get; set; }
        public int QuadrocopterId { get; set; }
        public int BasketId { get; set; }
    }
}
