﻿using System.ComponentModel.DataAnnotations.Schema;
using CommonInterface;

namespace DAL.Interface.DalModels
{
    [Table("CameraPresenses")]
    public class DalCameraPresense:IEntity
    {
        public int Id { get; set; }
        public string Type { get; set; }
    }
}
